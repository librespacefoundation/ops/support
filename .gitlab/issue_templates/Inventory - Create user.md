# Create Inventory user #

## Username ##

*Replace this line with the username*

## E-mail address ##

*Replace this line with the user's e-mail address*

## Roles

Please select all the roles you would like to have:

* [ ] Administrator - Adds/removes users
* [ ] Janitor - Keeps inventory clean and organized
* [ ] Stocker - Adds/deletes/modifies parts and stock with items as they arrive
* [ ] Builder - Uses parts and stock to build other parts
* [ ] Salesperson - Creates sales orders
* [ ] Purchaser - Creates purchase orders

/label ~Service::Inventory
/confidential
