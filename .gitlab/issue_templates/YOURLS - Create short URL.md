# Create short URL #

## URL ##

*Paste your long URL here*

## Type ##

 - [ ] Document
 - [ ] GitLab issue board
 - [ ] LSF project homepage
 - [ ] Matrix.org channel
 - [ ] Virtual meeting room
 - [ ] Other

## Domain ##

 - [ ] s.libre.space
 - [ ] satno.gs

## Shortened URL (optional) ##

*Replace this line with the proposed short URL*

/label ~Service::YOURLS
/confidential
