# Modify GitLab project developers #

## Project path ##

  * *Replace this line with the full project path; e.g. librespacefoundation/satnogs/satnogs-network*
  * *...*
  * *...*

## Add developers ##

  * *Replace this line with a developer's GitLab username; e.g. @acinonyx*
  * *...*
  * *...*

## Remove developers ##

  * *Replace this line with a developer's GitLab username; e.g. @acinonyx*
  * *...*
  * *...*

/label ~Service::GitLab
/confidential
