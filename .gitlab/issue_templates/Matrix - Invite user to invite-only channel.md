# Invite user to matrix invite-only channel #

## Channel ##

*Replace this line with the matrix channel to invite user to*

## User ##

*Replace this line with the matrix user to invite*

/label ~Service::Matrix
/confidential
