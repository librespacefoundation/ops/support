# Modify GitLab project settings #

## Project path ##

*Replace this line with the full project path; e.g. librespacefoundation/satnogs/satnogs-network*

## Settings ##

  - [ ] Private visibility
  - [ ] [Integrate with Matrix channel](https://docs.gitlab.com/ee/user/project/integrations/matrix.html) *add room name and options*

## Comments ##

*Replace this comment with any other information or setting that are not covered by the aforementioned placeholders or checklists*

/label ~Service::GitLab
/confidential
