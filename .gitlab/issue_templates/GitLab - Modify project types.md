# Modify project types #

## Project path ##

*Replace this line with the full project path; e.g. librespacefoundation/satnogs-comms*

## Project types ##

  - [ ] Organizational
    - [ ] with issue templates
  - [ ] Web application
  - [ ] Docker image
  - [ ] Generic software
  - [ ] Mechanical CAD
  - [ ] Electronics CAD
  - [ ] Documentation
  - [ ] Service desk

/label ~Service::GitLab
/confidential
