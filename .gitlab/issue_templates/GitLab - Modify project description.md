# Modify GitLab project description #

## Project path ##

*Replace this line with the full project path; e.g. librespacefoundation/satnogs/satnogs-network*

## Project description ##

*Replace this comment with the project description; e.g. Global Management Network for SatNOGS*

/label ~Service::GitLab
/confidential
