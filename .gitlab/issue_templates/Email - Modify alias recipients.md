# Modify email alias recipients #

## Alias address ##

*Replace this line with the email address of the alias; e.g. alias@libre.space*

## Add recipient addresses ##

  * *Replace this line with a recipient's email address; e.g. recipient1@libre.space*
  * *...*
  * *...*

## Remove recipient addresses ##

  * *Replace this line with a recipient's email address; e.g. recipient1@libre.space*
  * *...*
  * *...*

/label ~Service::Email
/confidential
